<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modul
 *
 * @author Muhammad Iqbal (市丸 零)
 */
class modul extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        self::tampil();
    }

    public function tampil($folder = 'panel', $halaman = 'beranda') {
        if ($this->sesiAkun !== FALSE) {
            $this->load->view($folder . '/' . $halaman);
        } else {
            $this->load->view('masuk');
        }
    }

}
