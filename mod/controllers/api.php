<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of api
 *
 * @author Muhammad Iqbal (市丸 零)
 */
class API extends CI_Controller {

    private $sesiAkun; // store session

    public function __construct() {
        parent::__construct();
        $this->sesiAkun = $this->session->userdata('_akun');
    }

    public function index() {
        redirect(site_url());
    }

    // provided by https://farizdotid.com/dokumentasi-api-daerah-indonesia/
    public function syncArea() {
        $hasil = 'Invalid URL';

        if ($this->sesiAkun !== FALSE) {
            $hasil = 'Gagal Proses';
            $responProvinsi = json_decode(self::_panggil('http://dev.farizdotid.com/api/daerahindonesia/provinsi'));

            if (!$responProvinsi->error) {
                if (self::_syncArea($responProvinsi->semuaprovinsi, 'provinsi')) {
                    foreach ($responProvinsi->semuaprovinsi as $provinsi) {
                        $responKabupaten = json_decode(self::_panggil('http://dev.farizdotid.com/api/daerahindonesia/provinsi/' . $provinsi->id . '/kabupaten'));

                        if (!$responKabupaten->error) {
                            if (self::_syncArea($responKabupaten->kabupatens, 'kabupaten', $provinsi->id)) {
                                foreach ($responKabupaten->kabupatens as $kabupaten) {
                                    $responKecamatan = json_decode(self::_panggil('http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/' . $kabupaten->id . '/kecamatan'));

                                    if (!$responKecamatan->error) {
                                        if (self::_syncArea($responKecamatan->kecamatans, 'kecamatan', $kabupaten->id)) {
                                            $hasil = 'Berhasil Proses';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        echo $hasil;
    }

    private function _panggil($url, $request = 'GET', $params = array()) {
        $response = '';
        $error = 'Invalid URL';

        if ($url != NULL) {
            $curl = curl_init();
            $curlData = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => TRUE, CURLOPT_ENCODING => '', CURLOPT_CUSTOMREQUEST => $request,
                CURLOPT_MAXREDIRS => 10, CURLOPT_TIMEOUT => 30, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1
            );
            curl_setopt_array($curl, $curlData);
            $response = curl_exec($curl);
            $error = curl_error($curl);
            curl_close($curl);
        }

        if ($error !== '') {
            $response = $error;
        }

        return $response;
    }

    private function _syncArea($params, $level, $idInduk = NULL) {
        $berhasil = FALSE;
        $rInduk = NULL;

        if ($idInduk != NULL) {
            $rInduk = $this->model->getRecord(array('table' => 'data_area', 'where' => array('entitas' => $idInduk, 'terpakai' => 1)));
        }

        foreach ($params as $areanya) {
            $rArea = $this->model->getRecord(array('table' => 'data_area', 'where' => array('entitas' => $areanya->id, 'terpakai' => 1)));
            $berhasil = $this->model->action(array(
                'table' => 'data_area', 'type' => ($rArea == NULL) ? $this->model->CREATE : $this->model->UPDATE,
                'data' => array(
                    'entitas' => ($rArea == NULL) ? $areanya->id : $rArea->entitas,
                    'kode' => ($rArea == NULL) ? random_string('unique') : $rArea->kode,
                    'nama' => strtoupper($areanya->nama), 'induk' => ($rInduk == NULL) ? '-' : $rInduk->kode, 'level' => $level,
                    'terpakai' => 1
                ), 'at' => array('entitas' => ($rArea == NULL) ? 0 : $rArea->entitas)
            ));

            if (!$berhasil) {
                break;
            }
        }

        return $berhasil;
    }

}

/* header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Methods: GET");
      header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding"); */