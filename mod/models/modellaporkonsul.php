<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modellaporkonsul
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelLaporKonsul extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_laporan_konsultan';
    }

    public function doAction($params) {
        unset($params['dt_basic_length']); // karena thoriq males setup advance UI nya ^_^
        $this->setValues($params);
        // overwrite
        $this->setValue('tanggal_survei', $this->formatdate->setDate($params['tanggal_survei-input']));
        $this->setValue('tanggal_susun', $this->formatdate->setDate($params['tanggal_susun-input']));

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'No Batch List Report',
            'rules' => 'trim|max_length[100]|required'
        );
        $urut = array(
            'field' => 'urut-input', 'label' => 'No Urut',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $provinsi = array(
            'field' => 'provinsi-input', 'label' => 'Data Provinsi',
            'rules' => 'trim|required'
        );
        $kabupaten = array(
            'field' => 'kabupaten-input', 'label' => 'Data Kabupaten',
            'rules' => 'trim|required'
        );
        $kecamatan = array(
            'field' => 'kecamatan-input', 'label' => 'Data Daerah',
            'rules' => 'trim|required'
        );
        $konsultan = array(
            'field' => 'konsultan-input', 'label' => 'Data Konsultan',
            'rules' => 'trim|required'
        );
        $bangunan = array(
            'field' => 'bangunan-input', 'label' => 'Data Sarana',
            'rules' => 'trim|required'
        );
        $pemimpin = array(
            'field' => 'pemimpin-input', 'label' => 'Nama Pimpinan',
            'rules' => 'trim|max_length[100]|required'
        );
        $tglSurvei = array(
            'field' => 'tanggal_survei-input', 'label' => 'Tanggal Survei',
            'rules' => 'trim|required'
        );
        $tglSusun = array(
            'field' => 'tanggal_susun-input', 'label' => 'Tanggal Penyusunan',
            'rules' => 'trim|required'
        );

        return array($kode, $nomor, $urut, $tglSurvei, $tglSusun, $provinsi, $kabupaten, $kecamatan, $konsultan, $bangunan, $pemimpin);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'urut' => 1, 'nomor' => strtoupper(random_string('alnum', 3) . '' . date('YmdHis')),
            'lampiran_foto' => '', 'lampiran_rks' => '', 'lampiran_rab' => '', 'lampiran_upl' => '',
            'sarana' => 'sekolah', 'kondisi' => 'rusak berat', 'bangunan' => '',
            'provinsi' => '', 'kabupaten' => '', 'kecamatan' => '', 'konsultan' => '', 'pemimpin' => '',
            'tanggal_survei' => '', 'tanggal_susun' => '', 'nominal' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nomor' => strtoupper($record->nomor),
                'pemimpin' => ucwords($record->pemimpin),
                'sarana' => $record->sarana, 'bangunan' => $record->bangunan,
                'provinsi' => $record->provinsi, 'kabupaten' => $record->kabupaten, 'kecamatan' => $record->kecamatan,
                'urut' => $record->urut, 'konsultan' => $record->konsultan,
                'tanggal_survei' => $this->formatdate->getDate($record->tanggal_survei, TRUE),
                'tanggal_susun' => $this->formatdate->getDate($record->tanggal_susun, TRUE),
                'lampiran_foto' => $record->lampiran_foto, 'lampiran_rks' => $record->lampiran_rks,
                'lampiran_rab' => $record->lampiran_rab, 'lampiran_upl' => $record->lampiran_upl,
                'nominal' => $this->toRp($record->nominal),
                'kondisi' => $record->kondisi,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $group = NULL;
        $where = array('terpakai' => 1);

        if ($query != NULL) {
            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);
                $where['kecamatan'] = $queries[1];

                if (intval($queries[0]) === 1) {
                    $where['kondisi'] = 'rusak berat';
                }
            } else {
                $where['nomor'] = $query;
            }
        } else {
            $group = 'nomor';
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'urut asc', 'group' => $group)) as $record) {
            $rSarana = $this->getRecord(array('table' => 'data_bangunan', 'where' => array('kode' => $record->bangunan, 'terpakai' => 1)));
            $rKonsul = $this->getRecord(array('table' => 'data_akun', 'where' => array('kode' => $record->konsultan, 'terpakai' => 1)));

            if ($rSarana != NULL && $rKonsul != NULL) {
                $data[] = array(
                    'kode' => $record->kode,
                    'nomor' => strtoupper($record->nomor),
                    'pemimpin' => ucwords($record->pemimpin),
                    'sarana' => strtoupper($rSarana->nama),
                    'tanggal_survei' => $this->formatdate->getDate($record->tanggal_survei),
                    'tanggal_susun' => $this->formatdate->getDate($record->tanggal_susun),
                    'nominal' => $this->toRp($record->nominal), 'nominal_int' => $record->nominal,
                    'alamat' => $rSarana->alamat . ' ' . (($rSarana->lon !== '') ? $rSarana->lon : '-') . ', ' . (($rSarana->lat !== '') ? $rSarana->lat : '-')
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
