<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelpaket
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelPaket extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_paket';
    }

    public function doAction($params) {
        $this->setValues($params);
        // overwrite
        $this->setValue('tanggal_lelang', $this->formatdate->setDate($params['tanggal_lelang-input']));
        $this->setValue('tanggal_dok', $this->formatdate->setDate($params['tanggal_dok-input']));

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $kegiatan = array(
            'field' => 'kegiatan-input', 'label' => 'Nama Kegiatan',
            'rules' => 'trim|max_length[100]|required'
        );
        $noDok = array(
            'field' => 'nomor_dok-input', 'label' => 'No SPBBJ',
            'rules' => 'trim|max_length[100]|required'
        );
        $tglLelang = array(
            'field' => 'tanggal_lelang-input', 'label' => 'Tanggal Lelang',
            'rules' => 'trim|required'
        );
        $tglDok = array(
            'field' => 'tanggal_dok-input', 'label' => 'Tanggal SPBBJ',
            'rules' => 'trim|required'
        );
        $provinsi = array(
            'field' => 'provinsi-input', 'label' => 'Data Provinsi',
            'rules' => 'trim|required'
        );
        $kabupaten = array(
            'field' => 'kabupaten-input', 'label' => 'Data Kabupaten',
            'rules' => 'trim|required'
        );
        $kecamatan = array(
            'field' => 'kecamatan-input', 'label' => 'Data Daerah',
            'rules' => 'trim|required'
        );
        $terpilih = array(
            'field' => 'terpilih-input', 'label' => 'Data Sarana',
            'rules' => 'trim|required'
        );
        $nilOri = array(
            'field' => 'nilai_asli-input', 'label' => 'Nominal Total',
            'rules' => 'trim|max_length[11]|integer|required'
        );
        $nilAju = array(
            'field' => 'nilai_ajuan-input', 'label' => 'Nominal Ajuan',
            'rules' => 'trim|max_length[11]|integer|required'
        );

        return array($kode, $kegiatan, $noDok, $tglDok, $tglLelang, $provinsi, $kabupaten, $kecamatan, $terpilih, $nilOri, $nilAju);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'kegiatan' => '', 'nomor_dok' => '', 'provinsi' => '', 'kabupaten' => '', 'kecamatan' => '',
            'tanggal_lelang' => '', 'tanggal_dok' => '', 'nilai_asli' => '', 'nilai_ajuan' => '',
            'status' => 'rencana', 'lampiran' => '', 'terpilih' => '',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'kegiatan' => ucwords($record->kegiatan),
                'nomor_dok' => strtoupper($record->nomor_dok),
                'provinsi' => $record->provinsi, 'kabupaten' => $record->kabupaten, 'kecamatan' => $record->kecamatan,
                'tanggal_lelang' => $this->formatdate->getDate($record->tanggal_lelang, TRUE),
                'tanggal_dok' => $this->formatdate->getDate($record->tanggal_dok, TRUE),
                'nilai_asli' => $this->toRp($record->nilai_asli), 'nilai_ajuan' => $this->toRp($record->nilai_ajuan),
                'lampiran' => $record->lampiran, 'status' => $record->status, 'terpilih' => $record->terpilih,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'nomor_dok asc')) as $record) {
            $data[] = array(
                'kode' => $record->kode,
                'kegiatan' => ucwords($record->kegiatan),
                'nomor_dok' => strtoupper($record->nomor_dok),
                'tanggal_lelang' => $this->formatdate->getDate($record->tanggal_lelang),
                'tanggal_dok' => $this->formatdate->getDate($record->tanggal_dok),
                'nilai_asli' => $this->toRp($record->nilai_asli), 'nilai_ajuan' => $this->toRp($record->nilai_ajuan),
                'status' => $record->status
            );
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
