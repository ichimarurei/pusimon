<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @author Muhammad Iqbal (市丸 零) <jms21maru@gmail.com> - Copyright (C) 2016
 */
class Otentikasi extends Model {

    private $userdata = array();

    public function __construct() {
        parent::__construct();
    }

    public function isValid($inputs) { // check for id/username
        $code = 0; // akses tidak sah (user undefined)

        if ($inputs['id-input'] == 'root' && $inputs['pin-input'] == 'b15m1ll4h') {
            // for root default: b15m1ll4h
            $code = 1; // user granted
            $this->userdata = array(
                '_akun' => random_string('unique'),
                '_nama' => 'Sys Dev',
                '_id' => 'root', '_otoritas' => 'admin'
            );
        } else {
            // get from profile account table
            $akun = $this->getRecord(array('table' => 'data_akun', 'where' => array('id' => $inputs['id-input'], 'pin' => $this->cryptorgram->encrypt($inputs['pin-input']), 'terpakai' => 1)));

            if ($akun != NULL) {
                $code = 1; // user granted
                $this->userdata = array(
                    '_akun' => $akun->kode, '_nama' => ucwords($akun->nama),
                    '_id' => $akun->id, '_otoritas' => $akun->otoritas
                );
            }
        }

        return $code;
    }

    public function getUserdata() {
        return $this->userdata;
    }

    public function getRules() {
        $id = array(
            'field' => 'id-input', 'label' => 'ID', 'rules' => 'trim|max_length[11]|required'
        );
        $pin = array(
            'field' => 'pin-input', 'label' => 'Sandi', 'rules' => 'trim|max_length[11]|required'
        );

        return array($id, $pin);
    }

}
