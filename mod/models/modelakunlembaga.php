<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelakunlembaga
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelAkunLembaga extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_akun_lembaga';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $akun = array(
            'field' => 'akun-input', 'label' => 'Data Akun',
            'rules' => 'trim|required'
        );
        $jabatan = array(
            'field' => 'jabatan-input', 'label' => 'Jabatan',
            'rules' => 'trim|max_length[50]|required'
        );
        $lampiran = array(
            'field' => 'lampiran-input', 'label' => 'Berkas Lampiran',
            'rules' => 'trim|max_length[255]'
        );

        return array($kode, $akun, $jabatan, $lampiran);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'akun' => '', 'jabatan' => '', 'lampiran' => '', 'lembaga' => 'pspop', 'tingkat' => 'balai',
            'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('akun' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'akun' => $record->akun,
                'jabatan' => strtoupper($record->jabatan),
                'lembaga' => $record->lembaga,
                'tingkat' => $record->tingkat,
                'lampiran' => $record->lampiran,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        return array();
    }

    public function getPilih($query) {
        return array();
    }

}
