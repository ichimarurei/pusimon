<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modeltag
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelTag extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_tag';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Tag',
            'rules' => 'trim|max_length[100]|required'
        );

        return array($kode, $nama);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'nama' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nama' => strtoupper($record->nama),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'sort' => 'nama asc')) as $record) {
            $data[] = array(
                'kode' => $record->kode,
                'nama' => strtoupper($record->nama)
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1), 'find' => array('nama' => $query), 'sort' => 'nama asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nama)));
        }

        return $data;
    }

}
