<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelarea
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelArea extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_area';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Nama Area',
            'rules' => 'trim|max_length[100]|required'
        );
        $induk = array(
            'field' => 'induk-input', 'label' => 'Induk Area',
            'rules' => 'trim|required'
        );
        $level = array(
            'field' => 'level-input', 'label' => 'Level Area',
            'rules' => 'trim|required'
        );

        return array($kode, $nama, $induk, $level);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'nama' => '', 'induk' => '', 'level' => 'provinsi', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));
        $prov = NULL;
        $kab = NULL;

        if ($record != null) {
            if ($record->level === 'kabupaten') {
                $prov = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $record->induk)));
            } else if ($record->level === 'kecamatan') {
                $kab = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $record->induk)));

                if ($kab != NULL) {
                    $prov = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kab->induk)));
                }
            }

            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nama' => strtoupper($record->nama), 'induk' => $record->induk, 'level' => $record->level,
                'induk_prov' => (($prov != NULL) ? $prov->kode : '-'),
                'induk_kab' => (($kab != NULL) ? $kab->kode : '-'),
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $where = array('terpakai' => 1);

        if ($query != NULL) {
            $where['induk'] = $query;
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'nama asc')) as $record) {
            $data[] = array(
                'kode' => $record->kode,
                'nama' => strtoupper($record->nama),
                'level' => strtoupper($record->level)
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $where = array('terpakai' => 1);

        if ($query != NULL) {
            $queries = array($query, NULL);

            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);
            }

            $where['induk'] = $queries[0];
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'find' => array('nama' => $queries[1]), 'sort' => 'nama asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nama)));
        }

        return $data;
    }

}
