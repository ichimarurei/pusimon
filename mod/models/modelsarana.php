<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelsarana
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelSarana extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_bangunan';
    }

    public function doAction($params) {
        $this->setValues($params);

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Nama Sarana',
            'rules' => 'trim|max_length[100]|required'
        );
        $alamat = array(
            'field' => 'alamat-input', 'label' => 'Alamat Sarana',
            'rules' => 'trim|required'
        );
        $tipe = array(
            'field' => 'tipe-input', 'label' => 'Jenis Sarana',
            'rules' => 'trim|required'
        );
        $provinsi = array(
            'field' => 'provinsi-input', 'label' => 'Data Provinsi',
            'rules' => 'trim|required'
        );
        $kabupaten = array(
            'field' => 'kabupaten-input', 'label' => 'Data Kabupaten',
            'rules' => 'trim|required'
        );
        $kecamatan = array(
            'field' => 'kecamatan-input', 'label' => 'Data Daerah',
            'rules' => 'trim|required'
        );
        $lon = array(
            'field' => 'lon-input', 'label' => 'Longitude',
            'rules' => 'trim|max_length[100]'
        );
        $lat = array(
            'field' => 'lat-input', 'label' => 'Latitude',
            'rules' => 'trim|max_length[100]'
        );

        return array($kode, $nama, $alamat, $tipe, $provinsi, $kabupaten, $kecamatan, $lon, $lat);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'nama' => '', 'alamat' => '', 'tipe' => 'sekolah', 'lon' => '', 'lat' => '',
            'provinsi' => '', 'kabupaten' => '', 'kecamatan' => '', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nama' => strtoupper($record->nama), 'alamat' => $record->alamat, 'tipe' => $record->tipe,
                'lon' => $record->lon, 'lat' => $record->lat,
                'provinsi' => $record->provinsi, 'kabupaten' => $record->kabupaten, 'kecamatan' => $record->kecamatan,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $where = array('terpakai' => 1);
        $tipenya = array('sekolah' => 'SEKOLAH', 'pasar' => 'PASAR', 'olga' => 'OLAHRAGA');
        $isNone = FALSE;

        if ($query != NULL) {
            $isNone = ($query === 'none');

            if (!$isNone) {
                $qMap = array('p' => 'provinsi', 'k' => 'kabupaten', 'd' => 'kecamatan');

                if (strpos($query, '___') !== FALSE) {
                    $queries = explode('___', $query);
                    $where['tipe'] = $queries[0];
                    $where[$qMap[$queries[1]]] = $queries[2];
                }
            }
        }

        if (!$isNone) {
            foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'sort' => 'nama asc')) as $record) {
                $isShow = TRUE;

                if (isset($queries[3])) {
                    $isShow = ($this->getRecord(array('table' => 'data_akun_konsultan', 'where' => array('akun' => $queries[3], 'bangunan' => $record->kode))) != NULL);
                }

                if ($isShow) {
                    $data[] = array(
                        'kode' => $record->kode,
                        'nama' => strtoupper($record->nama),
                        'alamat' => $record->alamat,
                        'tipe' => $tipenya[$record->tipe],
                        'gps' => (($record->lon !== '') ? $record->lon : '-') . ', ' . (($record->lat !== '') ? $record->lat : '-')
                    );
                }
            }
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $where = array('terpakai' => 1);

        if ($query != NULL) {
            $qMap = array('p' => 'provinsi', 'k' => 'kabupaten', 'd' => 'kecamatan');

            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);
                $where['tipe'] = $queries[0];
                $where[$qMap[$queries[1]]] = $queries[2];

                if (!isset($queries[3])) {
                    $queries[3] = NULL;
                }
            }
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'find' => array('nama' => $queries[3]), 'sort' => 'nama asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nama)));
        }

        return $data;
    }

}
