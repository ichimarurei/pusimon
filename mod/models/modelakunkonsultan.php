<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelakunkonsultan
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelAkunKonsultan extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_akun_konsultan';
    }

    public function doAction($params) {
        $this->setValues($params);
        // overwrite
        $this->setValue('tanggal_mulai', $this->formatdate->setDate($params['tanggal_mulai-input']));
        $this->setValue('tanggal_selesai', $this->formatdate->setDate($params['tanggal_selesai-input']));

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $akun = array(
            'field' => 'akun-input', 'label' => 'Data Akun',
            'rules' => 'trim|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'Nomor BA',
            'rules' => 'trim|max_length[50]|required'
        );
        $tglMulai = array(
            'field' => 'tanggal_mulai-input', 'label' => 'Tanggal Awal',
            'rules' => 'trim|required'
        );
        $tglSelesai = array(
            'field' => 'tanggal_selesai-input', 'label' => 'Tanggal Akhir',
            'rules' => 'trim|required'
        );
        $lampiran = array(
            'field' => 'lampiran-input', 'label' => 'Berkas Lampiran',
            'rules' => 'trim|max_length[255]'
        );
        $provinsi = array(
            'field' => 'provinsi-input', 'label' => 'Data Provinsi',
            'rules' => 'trim|required'
        );
        $kabupaten = array(
            'field' => 'kabupaten-input', 'label' => 'Data Kabupaten',
            'rules' => 'trim|required'
        );
        $kecamatan = array(
            'field' => 'kecamatan-input', 'label' => 'Data Daerah',
            'rules' => 'trim|required'
        );
        $bangunan = array(
            'field' => 'bangunan-input', 'label' => 'Data Sarana',
            'rules' => 'trim|required'
        );

        return array($kode, $akun, $nomor, $lampiran, $tglMulai, $tglSelesai, $provinsi, $kabupaten, $kecamatan, $bangunan);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'akun' => '', 'nomor' => '', 'lampiran' => '', 'sarana' => 'sekolah', 'tingkat' => 'balai', 'bangunan' => '',
            'provinsi' => '', 'kabupaten' => '', 'kecamatan' => '', 'tanggal_mulai' => '', 'tanggal_selesai' => '',
            'status' => 0, 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != null) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'akun' => $record->akun,
                'nomor' => strtoupper($record->nomor),
                'sarana' => $record->sarana, 'tingkat' => $record->tingkat,
                'tanggal_mulai' => $this->formatdate->getDate($record->tanggal_mulai, TRUE),
                'tanggal_selesai' => $this->formatdate->getDate($record->tanggal_selesai, TRUE),
                'provinsi' => $record->provinsi, 'kabupaten' => $record->kabupaten, 'kecamatan' => $record->kecamatan,
                'lampiran' => $record->lampiran, 'bangunan' => $record->bangunan, 'status' => $record->status,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'akun' => $query))) as $record) {
            $rSarana = $this->getRecord(array('table' => 'data_bangunan', 'where' => array('kode' => $record->bangunan, 'terpakai' => 1)));

            if ($rSarana != NULL) {
                $data[] = array(
                    'kode' => $record->kode,
                    'nama' => strtoupper($rSarana->nama)
                );
            }
        }

        return $data;
    }

    public function getPilih($query) {
        return array();
    }

}
