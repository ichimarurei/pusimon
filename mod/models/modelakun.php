<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of modelakun
 *
 * @author Muhammad Iqbal (市丸 零) <iqbal@indesc.com>
 */
class ModelAkun extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'data_akun';
    }

    public function doAction($params) {
        $this->setValues($params, array('id-input'));
        // overwrite
        $this->setValue('id', preg_replace('/\s+/', '', strtolower($params['id-input'])));
        $this->setValue('pin', $this->cryptorgram->encrypt($params['pin-input']));

        if (trim($params['tanggal_lahir-input']) != '') {
            $this->setValue('tanggal_lahir', $this->formatdate->setDate($params['tanggal_lahir-input']));
        }

        return $this->doSave();
    }

    public function getRules($action = '') {
        // init
        $kodeUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.kode]' : '';
        $idUnik = ($action == $this->CREATE) ? '|is_unique[' . $this->table . '.id]' : '';
        // rules
        $kode = array(
            'field' => 'kode-input', 'label' => 'Kode',
            'rules' => 'trim|max_length[32]|required' . $kodeUnik
        );
        $id = array(
            'field' => 'id-input', 'label' => 'ID',
            'rules' => 'trim|max_length[11]|required' . $idUnik
        );
        $pin = array(
            'field' => 'pin-input', 'label' => 'Sandi',
            'rules' => 'trim|max_length[11]|required'
        );
        $nomor = array(
            'field' => 'nomor-input', 'label' => 'No Identitas',
            'rules' => 'trim|max_length[50]'
        );
        $nama = array(
            'field' => 'nama-input', 'label' => 'Nama Lengkap',
            'rules' => 'trim|max_length[100]|required'
        );
        $tempat = array(
            'field' => 'tempat_lahir-input', 'label' => 'Tempat Lahir',
            'rules' => 'trim|max_length[50]'
        );
        $telepon = array(
            'field' => 'telepon-input', 'label' => 'No Telepon/HP',
            'rules' => 'trim|max_length[20]'
        );
        $email = array(
            'field' => 'email-input', 'label' => 'Email',
            'rules' => 'trim|max_length[30]|valid_email'
        );
        $foto = array(
            'field' => 'foto-input', 'label' => 'Photo',
            'rules' => 'trim|max_length[255]|required'
        );

        return array($kode, $id, $pin, $nomor, $nama, $tempat, $telepon, $email, $foto);
    }

    public function getData($kode) {
        $data = array(
            'key' => 0, 'kode' => random_string('unique'),
            'id' => '', 'pin' => '', 'otoritas' => 'lembaga',
            'nomor' => '', 'nama' => '', 'tempat_lahir' => '', 'tanggal_lahir' => '',
            'telepon' => '', 'email' => '', 'kelamin' => 'cowok', 'agama' => 'Islam', 'nikah' => 'belum',
            'alamat' => '', 'foto' => 'cowok.png', 'terpakai' => 1
        );
        $record = $this->getRecord(array('table' => $this->table, 'where' => array('kode' => $kode)));

        if ($record != NULL) {
            $data = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'id' => $record->id,
                'pin' => $this->cryptorgram->decrypt($record->pin),
                'otoritas' => $record->otoritas,
                'nomor' => strtoupper($record->nomor),
                'nama' => ucwords($record->nama),
                'tempat_lahir' => ucwords($record->tempat_lahir),
                'tanggal_lahir' => $this->formatdate->getDate($record->tanggal_lahir, TRUE),
                'telepon' => $record->telepon,
                'email' => $record->email,
                'kelamin' => $record->kelamin,
                'agama' => $record->agama,
                'nikah' => $record->nikah,
                'alamat' => $record->alamat,
                'foto' => $record->foto,
                'terpakai' => $record->terpakai
            );
        }

        return $data;
    }

    public function getTabel($query) {
        $data = array();
        $lembagaMap = array('pendidikan' => 'Kementrian Pendidikan', 'olga' => 'Kementrain Olahraga', 'pu' => 'Kementrian PU', 'pspop' => 'PSPOP');

        foreach ($this->getList(array('table' => $this->table, 'where' => array('terpakai' => 1, 'otoritas' => $query))) as $record) {
            $relasi = '';
            $rLembaga = NULL;

            if ($query === 'lembaga') {
                $rLembaga = $this->getRecord(array('table' => 'data_akun_lembaga', 'where' => array('akun' => $record->kode, 'terpakai' => 1)));

                if ($rLembaga != NULL) {
                    $relasi = strtoupper($rLembaga->jabatan) . ' (' . $lembagaMap[$rLembaga->lembaga] . ' - Tingkat ' . ucwords($rLembaga->tingkat) . ')';
                }
            } else if ($query === 'konsultan') {
                foreach ($this->getList(array('table' => 'data_akun_konsultan', 'where' => array('terpakai' => 1, 'akun' => $record->kode))) as $rKonsul) {
                    $rSarana = $this->getRecord(array('table' => 'data_bangunan', 'where' => array('kode' => $rKonsul->bangunan, 'terpakai' => 1)));

                    if ($rSarana != NULL) {
                        $relasi .= strtoupper($rSarana->nama) . ', ';
                    }
                }
            }

            $data[] = array(
                'key' => $record->entitas, 'kode' => $record->kode,
                'nama' => ucwords($record->nama),
                'telepon' => $record->telepon,
                'email' => $record->email,
                'relasi' => $relasi
            );
        }

        return $data;
    }

    public function getPilih($query) {
        $data = array();
        $where = array('terpakai' => 1);

        if ($query != NULL) {
            $queries = array($query, NULL);

            if (strpos($query, '___') !== FALSE) {
                $queries = explode('___', $query);
            }

            $where['otoritas'] = $queries[0];
        }

        foreach ($this->getList(array('table' => $this->table, 'where' => $where, 'find' => array('nama' => $queries[1]), 'sort' => 'nama asc')) as $record) {
            array_push($data, array('id' => $record->kode, 'text' => strtoupper($record->nama)));
        }

        return $data;
    }

}
