<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile>img {
                min-width: 151px;
                height: 170px;
                border: 5px solid #c4caca;
                margin-top: 30px;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Kelola Pengguna</li><li>Konsultan Lapangan</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Pengguna Konsultan Lapangan
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fas fa-pencil-alt"></i> </span>
                                    <h2>Data</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <form class="form-horizontal" id="model-form">
                                                <div class="col-sm-3">
                                                    <div class="col-sm-3 col-md-offset-3 profile ">
                                                        <input type="hidden" value="cowok.png" id="foto-input" name="foto-input">
                                                        <img id="img-profil" class="profile-user-img img-responsive img-circle" src="<?php echo base_url('file/html/img/cowok.png'); ?>" /><br>
                                                        <button class="btn btn-lg btn-default unggah-button" data-untuk="foto" type="button" data-toggle="modal" data-target="#modal-foto">Unggah Foto</button>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <input type="hidden" name="model-input" value="akun">
                                                    <input type="hidden" name="action-input" value="" id="ak-action-input"><!--jquery-->
                                                    <input type="hidden" name="key-input" value="" id="ak-key-input"><!--0/ajax-->
                                                    <input type="hidden" name="kode-input" value="" id="ak-kode-input">
                                                    <input type="hidden" name="terpakai-input" value="" id="ak-terpakai-input"><!--ajax-->
                                                    <input type="hidden" name="otoritas-input" value="konsultan">


                                                    <fieldset>
                                                        <legend><b>Data Pribadi</b></legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">No Identitas</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="NIP/KTP/SIM/Paspor/Lainnya" type="text" id="nomor-input" name="nomor-input" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Nama*</label>
                                                            <div class="col-md-10">
                                                                <input class="form-control" placeholder="Nama Lengkap" type="text" id="nama-input" name="nama-input" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tempat Lahir</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tempat Lahir" type="text" id="tempat_lahir-input" name="tempat_lahir-input" />
                                                            </div>
                                                            <label class="col-md-2 control-label">Tanggal Lahir</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Lahir" type="text" id="tanggal_lahir-input" name="tanggal_lahir-input" data-provide="datepicker" data-date-format="dd/mm/yyyy" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Telepon</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Nomor Telepon" type="text" id="telepon-input" name="telepon-input" />
                                                            </div>
                                                            <label class="col-md-2 control-label">Email</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Email" type="email" id="email-input" name="email-input" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Kelamin*</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="kelamin-input" name="kelamin-input">
                                                                    <option value="cowok">Laki-Laki</option>
                                                                    <option value="cewek">Perempuan</option>
                                                                </select>
                                                            </div>
                                                            <label class="col-md-2 control-label">Agama*</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="agama-input" name="agama-input">
                                                                    <option value="Islam">Islam</option>
                                                                    <option value="Katolik">Katholik</option>
                                                                    <option value="Protestan">Protestan</option>
                                                                    <option value="Hindu">Hindu</option>
                                                                    <option value="Budha">Budha</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Alamat</label>
                                                            <div class="col-md-10">
                                                                <textarea class="form-control text-tetap" placeholder="Alamat Lengkap" rows="4" id="alamat-input" name="alamat-input"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Status Pernikahan*</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                                    <option value="belum">Belum Menikah</option>
                                                                    <option value="nikah">Menikah</option>
                                                                    <option value="cerai">Janda / Duda</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <legend><b>Data Akun</b></legend>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">ID*</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="ID" type="text" id="id-input" name="id-input" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Sandi*</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Sandi" type="password" id="pin-input" name="pin-input" />
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <form class="form-horizontal" id="model2-form">
                                                    <input type="hidden" name="akun-input" value="" id="akun-input">
                                                    <input type="hidden" name="model-input" value="akunkonsultan">
                                                    <input type="hidden" name="status-input" value="0">
                                                    <input type="hidden" name="action-input" value="" id="lg-action-input"><!--jquery-->
                                                    <input type="hidden" name="key-input" value="" id="lg-key-input"><!--0/ajax-->
                                                    <input type="hidden" name="kode-input" value="" id="lg-kode-input">
                                                    <input type="hidden" name="bangunan-input" value="" id="bangunan-input">
                                                    <input type="hidden" id="lampiran-input" name="lampiran-input" value="">
                                                    <input type="hidden" name="terpakai-input" value="" id="lg-terpakai-input"><!--ajax-->
                                                    <fieldset>
                                                        <legend><b>Data Pendukung</b></legend>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Tingkat</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="tingkat-input" name="tingkat-input">
                                                                    <option value="direktorat">Direktorat</option>
                                                                    <option value="provinsi">Provinsi</option>
                                                                    <option value="balai">Balai</option>
                                                                </select>
                                                            </div>
                                                            <label class="col-md-2 control-label">Sarana</label>
                                                            <div class="col-md-4">
                                                                <select style="width:100%" class="select2 form-control" id="sarana-input" name="sarana-input">
                                                                    <option value="sekolah">Sekolah</option>
                                                                    <option value="pasar">Pasar</option>
                                                                    <option value="olahraga">Olah Raga</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Nomor</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Nomor BA" type="text" id="lg-nomor-input" name="nomor-input" />
                                                            </div>
                                                            <label class="col-md-2 control-label">Unggah</label>
                                                            <div class="col-md-4">
                                                                <button class="btn btn-default unggah-button" data-untuk="ba" type="button" data-toggle="modal" data-target="#modal-document">Lampiran BA</button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Durasi BA</label>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Awal" type="text" id="tanggal_mulai-input" name="tanggal_mulai-input" data-provide="datepicker" data-date-format="dd/mm/yyyy" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input class="form-control" placeholder="Tanggal Akhir" type="text" id="tanggal_selesai-input" name="tanggal_selesai-input" data-provide="datepicker" data-date-format="dd/mm/yyyy" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Lokasi</label>
                                                            <div class="col-md-3">
                                                                <select style="width:100%" class="select2 form-control" id="provinsi-input" name="provinsi-input">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select style="width:100%" class="select2 form-control" id="kabupaten-input" name="kabupaten-input">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select style="width:100%" class="select2 form-control" id="kecamatan-input" name="kecamatan-input">
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <button class="btn btn-info" type="button" id="cariin-button"><i class="fa fa-search"></i></button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table id="dt_basic1" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Lokasi Pencarian</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table id="dt_basic2" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Lokasi Dipilih</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a class="btn btn-default" href="<?php echo site_url('modul/tampil/akun/tabelBioKonsultan'); ?>">Batal</a>
                                                    <button class="btn btn-primary" type="button" id="simpan-button">
                                                        <i class="fa fa-save"></i>
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-foto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Untuk Document Upload -->
                            <div class="modal fade" id="modal-document">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedocument2" name="filedocument[]" class="file-loading" type="file" accept="*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>

        <script>
            var tabelhasil;

            $(document).ready(function () {
                var tabelnya = tablePencarian();
                tabelhasil = tableHasil();
                initUploader('#filedocument', 'file/app/avatar');
                initUploader('#filedocument2', 'file/app/lampiran');
                fillInputs();
                fillInputsAkun('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>', tabelhasil);

                $("#cariin-button").click(function (e) {
                    if ($('#provinsi-input').val() !== null && $('#kabupaten-input').val() !== null && $('#kecamatan-input').val() != null) {
                        var urlParam = $('#sarana-input').val() + '___d___' + $('#kecamatan-input').val();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/sarana'); ?>/" + urlParam).load();
                        tabelnya.columns.adjust().draw();
                    }
                });
                $("#simpan-button").click(function (e) {
                    doSave("Akun", "<?php echo site_url('modul/tampil/akun/tabelBioKonsultan'); ?>", $('#model-form'));
                });
            });

            function fillInputs() {
                $('#provinsi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Provinsi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/area/-'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#kabupaten-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kabupaten'});
                    }
                });

                $('#kecamatan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kecamatan'});
                    }
                });

                $('#provinsi-input').change(function () {
                    var kode = $(this).val();
                    $('#kabupaten-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kabupaten'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#kabupaten-input').change(function () {
                    var kode = $(this).val();
                    $('#kecamatan-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kecamatan'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });
            }

            function tablePencarian() {
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/sarana'); ?>",
                    "columns": [
                        {'data': 'nama'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<button class="actionBtn btn btn-info btn-flat" data-aksi="' + row.kode + '">Pilih</button>';
                            }, "targets": [1]
                        }
                    ]
                });

                return $("#dt_basic1").DataTable(dt_options);
            }

            function tableHasil() {
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/akunkonsultan/0'); ?>",
                    "columns": [
                        {'data': 'nama'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<i class="fas fa-check"></i>';
                            }, "targets": [1]
                        }
                    ]

                });

                return $("#dt_basic2").DataTable(dt_options);
            }

            $(document).on('click', '.actionBtn', function () {
                fillInputsKonsultan($(this).data('aksi'));
            });

            function initUploader(id, path_file) {
                var filename = '';
                $(id).fileinput({
                    maxFileCount: 1, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-default btn-flat", browseLabel: "Browse", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-warning btn-flat", removeLabel: "Delete", removeIcon: '<i class="fas fa-trash"></i> ',
                    uploadClass: "btn btn-primary btn-flat", uploadLabel: "Upload", uploadIcon: '<i class="fa fa-cloud-upload-alt"></i> ',
                    showCaption: false,
                    uploadExtraData: function () {
                        return {path: path_file, dir: $('#ak-kode-input').val()};
                    }
                });

                $(id).on('fileloaded', function (event, file, previewId, index, reader) {
                    filename = file.name.replace(/\s/g, '_');
                });

                $(id).on('fileuploaded', function () {
                    $(id).fileinput('clear');

                    if (id == '#filedocument') {
                        $('#foto-input').val(filename);
                        $('#modal-foto').modal('hide');

                        $('#img-profil').attr('src', '<?php echo base_url('file/app/avatar'); ?>/' + $('#ak-kode-input').val() + '/' + filename);
                    } else {
                        $('#lampiran-input').val(filename);
                        $('#modal-document').modal('hide');

                    }
                });
            }

            function fillInputsAkun(param, tabelnya) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=akun&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        // Data Pribadi
                        $('#nomor-input').val(response['data'].nomor);
                        $('#nama-input').val(response['data'].nama);
                        $('#tempat_lahir-input').val(response['data'].tempat_lahir);
                        $('#tanggal_lahir-input').val(response['data'].tanggal_lahir);
                        $('#telepon-input').val(response['data'].telepon);
                        $('#email-input').val(response['data'].email);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#kelamin-input').val(response['data'].kelamin).trigger('change');
                        $('#agama-input').val(response['data'].agama).trigger('change');
                        $('#nikah-input').val(response['data'].nikah).trigger('change');

                        // Data Akun
                        $('#id-input').val(response['data'].id);
                        $('#pin-input').val(response['data'].pin);

                        // Data Hidden
                        $('#akun-input').val(response['data'].kode);
                        $('#foto-input').val(response['data'].foto);
                        $('#ak-action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#ak-key-input').val(response['data'].key);
                        $('#ak-kode-input').val(response['data'].kode);
                        $('#ak-terpakai-input').val(response['data'].terpakai);

                        if (response['data'].key > 0) {
                            if (response['data'].foto !== 'cowok.png') {
                                $('#img-profil').attr('src', '<?php echo base_url('file/app/avatar'); ?>/' + response['data'].kode + '/' + response['data'].foto);
                            }
                        }

                        tabelnya.ajax.url("<?php echo site_url('data/tabel/akunkonsultan'); ?>/" + response['data'].kode).load();
                        tabelnya.columns.adjust().draw();
                    }
                });
            }

            function fillInputsKonsultan(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=akunkonsultan&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#kode-input').val(param);
                        $('#bangunan-input').val(param);
                        // Data Hidden
                        $('#lg-action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#lg-key-input').val(response['data'].key);
                        $('#lg-kode-input').val(param);
                        $('#lg-terpakai-input').val(response['data'].terpakai);
                        $.when(doSaveCallback($('#model2-form'))).done(function (response) {
                            var title = 'Success';
                            var message = '';
                            var icon = 'warning';

                            if (response['return'].code === 0) {
                                title = 'Eror';
                                icon = 'error';
                                message = 'Error !!!';
                            } else if (response['return'].code === 1) {
                                title = 'Berhasil';
                                icon = 'success';
                                message = 'Data Berhasil Disimpan';
                            } else if (response['return'].code === 2) {
                                title = 'Gagal';
                                icon = 'error';
                                message = 'Data Gagal Disimpan';
                            } else if (response['return'].code === 3) {
                                title = 'Perhatian !!!';
                                icon = 'warning';
                                message = response['return'].message;
                            }

                            swal({
                                title: title,
                                html: message,
                                timer: 3000,
                                type: icon,
                                showConfirmButton: false,
                                onClose: function () {
                                    $.unblockUI();
                                    tabelhasil.ajax.url("<?php echo site_url('data/tabel/akunkonsultan'); ?>/" + $('#ak-kode-input').val()).load();
                                    tabelhasil.columns.adjust().draw();
                                }
                            });
                        });
                    }
                });
            }
        </script>
    </body>
</html>