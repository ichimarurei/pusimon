<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile>img {
                min-width: 151px;
                height: 170px;
                border: 5px solid #c4caca;
                margin-top: 30px;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Master</li><li>Data PSPOP Sekolah</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Data PSPOP Sekolah
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fas fa-pencil-alt"></i> </span>
                                    <h2>Data PSPOP Sekolah</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <form class="form-horizontal" id="model-form">
                                                <input type="hidden" name="model-input" value="paket">
                                                <input type="hidden" name="action-input" value="" id="action-input"><!--jquery-->
                                                <input type="hidden" name="key-input" value="" id="key-input"><!--0/ajax-->
                                                <input type="hidden" name="kode-input" value="" id="kode-input">
                                                <input type="hidden" name="terpakai-input" value="" id="terpakai-input"><!--ajax-->

                                                <input type="hidden" name="terpilih-input" value="-" id="terpilih-input"><!--ajax-->

                                                <div class="col-sm-12">
                                                    <legend><b>Data Pribadi</b></legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nama Kegiatan</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" placeholder="Nama Kegiatan Perencanaan" type="text" id="kegiatan-input" name="kegiatan-input" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Tanggal Lelang</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control" placeholder="Tanggal Proses Lelang" type="text" id="tanggal_lelang-input" name="tanggal_lelang-input"  data-provide="datepicker" data-date-format="dd/mm/yyyy"/>
                                                        </div>
                                                        <label class="col-md-2 control-label">Status Lelang</label>
                                                        <div class="col-md-3">
                                                            <select style="width:100%" class="select2 form-control" id="status-input" name="status-input">
                                                                <option value="rencana">Rencana</option>
                                                                <option value="lakukan">Lakukan</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <legend><b>SPBBJ</b></legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">No SPBBJ</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control" placeholder="No SPBBJ" type="text" id="nomor_dok-input" name="nomor_dok-input"/>
                                                        </div>
                                                        <label class="col-md-2 control-label">Tanggal SPBBJ</label>
                                                        <div class="col-md-3">
                                                            <input class="form-control" placeholder="Tanggal SPBBJ" type="text" id="tanggal_dok-input" name="tanggal_dok-input" data-provide="datepicker" data-date-format="dd/mm/yyyy"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Unggah Berkas</label>
                                                        <div class="col-md-4">
                                                            <input type="hidden" name="lampiran-input" id="lampiran-input" value="">
                                                            <button class="btn btn-default unggah-button" data-untuk="ba" type="button" data-toggle="modal" data-target="#modal-document">Unggah</button>
                                                        </div>
                                                    </div>
                                                    
                                                    <legend><b>Paket</b></legend>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Lokasi</label>
                                                        <div class="col-md-3">
                                                            <select style="width:100%" class="select2 form-control" id="provinsi-input" name="provinsi-input">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select style="width:100%" class="select2 form-control" id="kabupaten-input" name="kabupaten-input">
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select style="width:100%" class="select2 form-control" id="kecamatan-input" name="kecamatan-input">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nilai Total</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Nilai Total" type="text" id="nilai_asli-input" name="nilai_asli-input" readonly/>
                                                        </div>
                                                        <label class="col-md-1 control-label">Nilai Ajuan</label>
                                                        <div class="col-md-4">
                                                            <input class="form-control nominal-text" placeholder="Nilai Ajuan" type="text" id="nilai_ajuan-input" name="nilai_ajuan-input" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-1 col-md-offset-10">
                                                            <button style="width:100%" class="btn btn-info unggah-button" type="button" id="saring-button">Saring</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Lokasi</th>
                                                            <th>Alamat</th>
                                                            <th>Nominal</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a class="btn btn-default" href="<?php echo site_url('modul/tampil/pspop/tabelSekolah'); ?>">Batal</a>
                                                    <button class="btn btn-primary" type="button" id="simpan-button">
                                                        <i class="fa fa-save"></i>
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- Modal Untuk Berkas -->
                            <div class="modal fade" id="modal-foto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedoc-foto" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var pilih_PSPOP_kode = [];
            var pilih_PSPOP_nominal = [];

            var tableFilter;

            $(document).ready(function () {

                fillWilayahInputs();
                fillInputsData('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');
                tableFilter = tableFilterPaket();

                events();
                function events(){

                    $(".nominal-text").keyup(function () {
                        var text = formatRp($(this).val());
                        $(this).val(text);
                    });

                    $('#saring-button').click(function(e){
                        tableFilter.ajax.url("<?php echo site_url('data/tabel/laporkonsul/1___'); ?>" + $('#kecamatan-input').val()).load();
                        tableFilter.columns.adjust().draw();
                    });

                    $('#simpan-button').click(function(e){
                        $('#terpilih-input').val(pilih_PSPOP_kode.join('___'));                    
                        nominalFormats();
                        doSave("paket", "<?php echo site_url('modul/tampil/pspop/tabelSekolah'); ?>", $('#model-form'));
                    });

                    $(document).on('click', '.actionBtn', function (e) {
                        pilihPSPOP($(this).data('kode'), $(this).data('nominal'));

                        if($(this).hasClass('btn-info')){
                            $(this).removeClass('btn-info');
                            $(this).val('Dipilih')
                            $(this).addClass('btn-success');
                        }else{
                            $(this).addClass('btn-info');
                            $(this).val('Pilih')
                            $(this).removeClass('btn-success');
                        }

                    });

                }

            });

            function pilihPSPOP(kode, nominal){
                if(!pilih_PSPOP_kode.includes(kode)){
                    pilih_PSPOP_kode.push(kode);
                    pilih_PSPOP_nominal.push(nominal);
                }else{
                    for( var i = 0; i < pilih_PSPOP_kode.length; i++){ 
                        if ( pilih_PSPOP_kode[i] === kode) {
                            pilih_PSPOP_kode.splice(i, 1); 
                            pilih_PSPOP_nominal.splice(i, 1);
                        }
                    }
                }

                var total = 0;
                for( var i = 0; i < pilih_PSPOP_kode.length; i++){ 
                    total = total + pilih_PSPOP_nominal[i];
                }
                $('#nilai_asli-input').val(formatRp(total.toString()));

                console.log(pilih_PSPOP_kode);
                console.log(pilih_PSPOP_nominal);
            }
            
            function fillInputsData(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=paket&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        
                        $('#kegiatan-input').val(response['data'].kegiatan);
                        $('#tanggal_lelang-input').val(response['data'].tanggal_lelang);
                        $('#status-input').val(response['data'].status);

                        $('#nomor_dok-input').val(response['data'].nomor_dok);
                        $('#tanggal_dok-input').val(response['data'].tanggal_dok);
                        $('#lampiran-input').val(response['data'].lampiran);

                        $('#nilai_asli-input').val(response['data'].nilai_asli);
                        $('#nilai_ajuan-input').val(response['data'].nilai_ajuan);



                        // Data Hidden
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#terpakai-input').val(response['data'].terpakai);

                        if (response['data'].key > 0) {

                            var pilihProvinsi = $('#provinsi-input');
                            var pilihKecamatan = $('#kecamatan-input');
                            var pilihKabupaten = $('#kabupaten-input');
                            
                            pilih_PSPOP_kode = response['data'].terpilih.split('___');
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].provinsi,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProvinsi.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProvinsi.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kabupaten,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKabupaten.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKabupaten.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kecamatan,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKecamatan.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKecamatan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            tableFilter.ajax.url("<?php echo site_url('data/tabel/laporkonsul/1___'); ?>" + response['data'].kecamatan).load();
                            tableFilter.columns.adjust().draw();

                            console.log(pilih_PSPOP_nominal);
                        }
                    }
                });
            }

            function fillWilayahInputs() {
                $('#provinsi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Provinsi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/area/-'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#kabupaten-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kabupaten'});
                    }
                });

                $('#kecamatan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kecamatan'});
                    }
                });

                $('#provinsi-input').change(function () {
                    var kode = $(this).val();
                    $('#kabupaten-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kabupaten'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#kabupaten-input').change(function () {
                    var kode = $(this).val();
                    $('#kecamatan-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kecamatan'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

            }

            function tableFilterPaket(){
                let dt_options = $.extend({}, globalDTOptions, {
                    "columns": [
                        {'data': 'sarana'},
                        {'data': 'alamat'},
                        {'data': 'nominal'},
                        {'width': '5%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                var btnClass = 'btn-info'; var btnText = 'Pilih';
                                if (pilih_PSPOP_kode.includes(row.kode)){
                                    btnClass = 'btn-success'; btnText = 'Dipilih';
                                    pilih_PSPOP_nominal[pilih_PSPOP_kode.indexOf(row.kode)] = parseInt(row.nominal_int);
                                }
                                
                                return '<input type="button" class="actionBtn btn '+ btnClass +' btn-flat" '+
                                'data-kode="'+ row.kode +'" data-nominal="'+ row.nominal_int +'" value="'+ btnText +'">';
                            }, "targets": [3]
                        }
                    ]

                });
                return $("#dt_basic").DataTable(dt_options);
            }

        </script>
    </body>
</html>