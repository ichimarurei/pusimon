<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url('file/html/js/plugin/pace/pace.min.js'); ?>"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="<?php echo base_url('file/html/js/libs/jquery-3.2.1.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/jquery-ui.min.js'); ?>"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url('file/html/js/app.config.js'); ?>"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url('file/html/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js'); ?>"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url('file/html/js/bootstrap/bootstrap.min.js'); ?>"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url('file/html/js/notification/SmartNotification.min.js'); ?>"></script>
<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url('file/html/js/smartwidgets/jarvis.widget.min.js'); ?>"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url('file/html/js/plugin/select2/select2.min.js'); ?>"></script>
<!-- JQUERY UI  Bootstrap Slider -->
<script src="<?php echo base_url('file/html/js/plugin/bootstrap-slider/bootstrap-slider.min.js'); ?>"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url('file/html/js/plugin/msie-fix/jquery.mb.browser.min.js'); ?>"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url('file/html/js/plugin/fastclick/fastclick.min.js'); ?>"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url('file/html/js/plugin/jquery-validate/jquery.validate.min.js'); ?>"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url('file/html/js/plugin/masked-input/jquery.maskedinput.min.js'); ?>"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url('file/html/js/app.min.js'); ?>"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<!-- File Input -->
<script src="<?php echo base_url('file/html/plugin/bootstrap-fileinput/js/fileinput.min.js'); ?>"></script>
<!-- Bootstrap Summernote -->
<script type="text/javascript" src="<?php echo base_url('file/html/plugin/summernote/summernote.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/plugin/datepicker/datepicker.js'); ?>"></script>
<!-- EASY PIE CHARTS -->
<script src="<?php echo base_url('file/html/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js'); ?>"></script>
<!-- SPARKLINES -->
<script src="<?php echo base_url('file/html/js/plugin/sparkline/jquery.sparkline.min.js'); ?>"></script>
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.cust.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.resize.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.fillbetween.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.orderBar.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.pie.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.time.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/flot/jquery.flot.tooltip.min.js'); ?>"></script>
<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="<?php echo base_url('file/html/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<!-- Full Calendar -->
<script src="<?php echo base_url('file/html/js/plugin/moment/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/fullcalendar/fullcalendar.min.js'); ?>"></script>
<!-- DatePicker -->
<script src="<?php echo base_url('file/html/plugin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/plugin/bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min.js'); ?>"></script>
<!-- Morris Chart Dependencies -->
<script src="<?php echo base_url('file/html/js/plugin/morris/raphael.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/morris/morris.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/highChartCore/highcharts-custom.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/js/plugin/highchartTable/jquery.highchartTable.min.js'); ?>"></script>
<!-- Chart.js Dependencies -->
<script src="<?php echo base_url('file/html/plugin/chart.js/chart.min.js'); ?>"></script>
<script src="<?php echo base_url('file/html/plugin/chart.js/utils.js'); ?>"></script>
<!-- Map -->
<script src="<?php echo base_url('file/html/plugin/leaflet/leaflet.js'); ?>"></script>
<!-- Sweetalert -->
<script src="<?php echo base_url('file/html/plugin/sweetalert2.js/sweetalert2.min.js'); ?>"></script>
<!-- jQuery blockUI -->
<script src="<?php echo base_url('file/html/plugin/sweetalert2.js/jquery.blockUI.js'); ?>"></script>

<script>
    runAllForms();
    pageSetUp();

    $(document).keypress(function (e) {
        if (e.which === 13) { // enter key press event
            return (e.target.nodeName.toLowerCase() === 'textarea'); // disable enter event except for textarea
        }
    });

    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(document).ready(function () {
        $('.tglpicker').datepicker({// Date picker
            format: 'dd/mm/yyyy',
            autoHide: true
        });
    });

    function doSave(text, url, form) {
        $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false,
            success: function (response) {
                var title = 'Success';
                var message = '';
                var icon = 'warning';

                if (response['return'].code === 0) {
                    title = 'Eror';
                    icon = 'error';
                    message = 'Error !!!';
                } else if (response['return'].code === 1) {
                    title = 'Berhasil';
                    icon = 'success';
                    message = 'Data Berhasil ' + text;
                } else if (response['return'].code === 2) {
                    title = 'Gagal';
                    icon = 'error';
                    message = 'Data Gagal ' + text;
                } else if (response['return'].code === 3) {
                    title = 'Perhatian !!!';
                    icon = 'warning';
                    message = response['return'].message;
                }

                swal({
                    title: title,
                    html: message,
                    timer: 3000,
                    type: icon,
                    showConfirmButton: false,
                    onClose: function () {
                        $.unblockUI();

                        if (response['return'].code === 1) {
                            $(location).attr('href', url);
                        }
                    }
                });
            }
        });
    }

    function doSaveCallback(form) {
        return $.ajax({
            url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
            dataType: 'json', type: 'POST', cache: false
        });
    }

    function formatRp(angka) {
        var number_string = angka.replace(/[^,\d]/g, '').toString();
        var split = number_string.split(',');
        var sisa = split[0].length % 3;
        var rupiah = split[0].substr(0, sisa);
        var ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        var separator;

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = (split[1] !== undefined) ? rupiah + ',' + split[1] : rupiah;
        return (rupiah ? 'Rp. ' + rupiah : '');
    }

    function nominalFormats() {
        $('.nominal-text').each(function (i, obj) {
            $(obj).val(rpToNum($(obj).val()));
        });
    }

    function rpToNum(nominal) {
        var nominalText = nominal.replace('Rp. ', '');

        return nominalText.replace(/\./g, '');
    }

    // duplication image processing
    function setImageAs(img, as, dir) {
        return $.ajax({
            url: "<?php echo site_url('data/salinFoto'); ?>", data: 'img=' + img + '&dir=' + dir + '&as=' + as,
            dataType: 'json', type: 'POST', cache: false
        });
    }

    // retrieve galleries
    function lihatGaleri(dir) {
        return $.ajax({
            url: "<?php echo site_url('data/galeri'); ?>", data: 'dir=' + dir,
            dataType: 'json', type: 'POST', cache: false
        });
    }
</script>