<meta charset="utf-8">
<title>PU | SIM Pembangunan</title>
<meta name="description" content="PU | SIM Pembangunan">
<meta name="author" content="Muhammad Iqbal (市丸 零) <iqbal@indesc.com>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/fonts/fontawesome-5.11.2/css/fontawesome.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/fonts/fontawesome-5.11.2/css/regular.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/fonts/fontawesome-5.11.2/css/solid.min.css'); ?>">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/smartadmin-production-plugins.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/smartadmin-production.min.css'); ?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/smartadmin-skins.min.css'); ?>">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/smartadmin-rtl.min.css'); ?>">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="<?php echo base_url('file/html/img/favicon/pu.ico'); ?>" type="image/x-icon">
<link rel="icon" href="<?php echo base_url('file/html/img/favicon/pu.ico'); ?>" type="image/x-icon">

<!-- #GOOGLE FONT -->
<link rel="stylesheet" href="<?php echo base_url('file/html/css/googlefonts.css'); ?>">

<!-- Sweetalert2 -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/sweetalert2.js/sweetalert2.min.css'); ?>">
<!-- File Input -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/bootstrap-fileinput/css/fileinput.min.css'); ?>">
<!-- DatePicker -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
<!-- TimePicker -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/bootstrap-timepicker/css/bootstrap-timepicker.min.css'); ?>">
<!-- Lightbox -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/lightbox/ekko-lightbox.css'); ?>">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/select2/css/select2.min.css'); ?>">
<!-- Map -->
<link rel="stylesheet" href="<?php echo base_url('file/html/plugin/leaflet/leaflet.css'); ?>" />
<style>
    .text-tetap {
        resize: none;
    }
    .select2-selection__arrow b::before {
        content: none;
    }
    .select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 32px;
    }
    .menu-on-top .menu-item-parent{
        max-width: none;
    }
    .menu-on-top nav ul ul li:hover>a:after, 
    .menu-on-top nav>ul ul>li a:after, 
    .menu-on-top nav>ul ul>li a:hover:after, 
    .menu-on-top nav>ul>li>a:after{
        font-family: "Font Awesome 5 Free"; font-weight: 900;
    }
</style>