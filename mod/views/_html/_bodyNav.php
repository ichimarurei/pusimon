<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <nav>
        <ul>
            <li>
                <a href="<?php echo site_url(); ?>"><i class="fas fa-home"></i> <span class="menu-item-parent"> Beranda </span></a>
            </li>
            <li>
                <a href="#" title="Kelola Pengguna Sistem"><i class="fas fa-users"></i> <span class="menu-item-parent"> Kelola Pengguna </span></a>
                <ul>
                    <li>
                        <a href="#" title="Pengguna Kementerian / Lembaga">Kementerian / Lembaga</a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/akun/dataBioLembaga'); ?>" title="Pendataan Pengguna Kementerian / Lembaga"><span class="menu-item-parent"> Pendataan </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/akun/tabelBioLembaga'); ?>" title="Tabel/Daftar Pengguna Kementerian / Lembaga"><span class="menu-item-parent"> Tabel/Daftar </span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="Pengguna Konsultan Lapangan">Konsultan Lapangan</a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/akun/dataBioKonsultan'); ?>" title="Pendataan Pengguna Konsultan Lapangan"><span class="menu-item-parent"> Pendataan </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/akun/tabelBioKonsultan'); ?>" title="Tabel/Daftar Pengguna Konsultan Lapangan"><span class="menu-item-parent"> Tabel/Daftar </span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" title="Konsultan Lapangan"><i class="fas fa-user-secret"></i> <span class="menu-item-parent"> Konsultan Lapangan </span></a>
                <ul>
                    <li>
                        <a href="#" title="Laporan Konsultan Lapangan">Laporan</a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/konsultan/dataLaporan'); ?>" title="Pendataan Laporan Konsultan Lapangan"><span class="menu-item-parent"> Pendataan </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/konsultan/tabelLaporan'); ?>" title="Tabel/Daftar Laporan Konsultan Lapangan"><span class="menu-item-parent"> Tabel/Daftar </span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" title="PSPOP"><i class="fas fa-building"></i> <span class="menu-item-parent"> PSPOP </span></a>
                <ul>
                    <li>
                        <a href="#" title="Pembuatan Paket">Pembuatan Paket</a>
                        <ul>
                            <li>
                                <a href="#" title="Pembuatan Paket ...">Sekolah</a>
                                <ul>
                                    <li>
                                        <a href="<?php echo site_url('modul/tampil/pspop/dataSekolah'); ?>" title="Pendataan ..."><span class="menu-item-parent"> Pendataan </span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('modul/tampil/pspop/tabelSekolah'); ?>" title="Tabel/Daftar ..."><span class="menu-item-parent"> Tabel/Daftar </span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0);" title="Pembuatan Paket ...">Pasar (Fase Development!)</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" title="Pembuatan Paket ...">Olah Raga (Fase Development!)</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" title=""Data Utama><i class="fas fa-database"></i> <span class="menu-item-parent">Data Master</span></a>
                <ul>
                    <li>
                        <a href="#" title="Data Master Area">Data Area</a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/master/dataArea'); ?>" title="Pendataan Data Master Area"><span class="menu-item-parent"> Pendataan </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/master/tabelArea'); ?>" title="Tabel/Daftar Data Master Area"><span class="menu-item-parent"> Tabel/Daftar </span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="Data Master Bangunan">Data Bangunan</a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/master/dataBangunan'); ?>" title="Pendataan Data Master Bangunan"><span class="menu-item-parent"> Pendataan </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('modul/tampil/master/tabelBangunan'); ?>" title="Tabel/Daftar Data Master Bangunan"><span class="menu-item-parent"> Tabel/Daftar </span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo site_url('modul/tampil/master/dataTag'); ?>" title="Pendataan Data Master Tag"><span class="menu-item-parent">Data Tag</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo site_url('akses/untuk/keluar'); ?>"><i class="fa fa-sign-out-alt"></i> <span class="menu-item-parent"> Keluar </span></a>
            </li>
        </ul>
    </nav>

    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<!-- END NAVIGATION -->