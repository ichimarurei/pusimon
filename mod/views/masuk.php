<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us" id="lock-page">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <!-- page related CSS -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('file/html/css/lockscreen.min.css'); ?>">
    </head>

    <body>
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <form class="lockscreen animated flipInY" id="login-form">
                <div class="logo">
                    <h1 class="semi-bold"><img src="<?php echo base_url('file/html/img/logo-o.png'); ?>" alt="" /> SIM Pembangunan</h1>
                </div>
                <div>
                    <img src="<?php echo base_url('file/html/img/pu.png'); ?>" alt="" width="120" height="120" />
                    <div>
                        <h1><i class="fa fa-shield-alt fa-3x text-muted air air-top-right hidden-mobile"></i>Admin Panel<small><i class="fa fa-shield-alt text-muted"></i> Akses Masuk</small></h1>
                        <div class="row">
                            <div class="col-xs-6">
                                <input class="form-control" type="text" id="id-input" name="id-input" placeholder="ID" autofocus>
                            </div>
                            <div class="col-xs-6">
                                <input class="form-control" type="password" id="pin-input" name="pin-input" placeholder="Sandi">
                            </div>
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-primary btn-block" type="button" id="submit-btn">
                                    <i class="fa fa-key"></i> Masuk
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="font-xs margin-top-5">Panel Admin © 2019</p>
            </form>
        </div>

        <!--================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).keypress(function (e) {
                if (e.which === 13) { // enter key press event
                    if ($('#id-input').val().trim() === '') {
                        $('#id-input').focus();
                    } else if ($('#pin-input').val().trim() === '') {
                        $('#pin-input').focus();
                    } else {
                        goSend();
                    }
                }
            });

            $(document).ready(function () {
                $('#submit-btn').on('click', function () {
                    goSend();

                    return false;
                });
            });

            function goSend() {
                $.blockUI({message: '<h1>Proses Otentikasi...</h1>'});
                $.ajax({
                    url: "<?php echo site_url('akses/untuk/masuk'); ?>",
                    data: $("#login-form").serialize(),
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    success: function (respon) {
                        $.unblockUI();

                        if (respon['data'].code === 1) {
                            swal({
                                title: 'Berhasil',
                                text: respon['data'].message,
                                timer: 3000,
                                type: 'success',
                                showConfirmButton: false,
                                onClose: function () {
                                    $(location).attr('href', "<?php echo site_url(); ?>");
                                }
                            });
                        } else {
                            swal({
                                title: 'Gagal',
                                html: '<p>' + respon['data'].message + '</p>',
                                timer: (respon['data'].code === 2) ? 3000 : 2000,
                                type: (respon['data'].code === 2) ? 'warning' : 'error',
                                showConfirmButton: false
                            });
                        }
                    }
                });
            }
        </script>
    </body>
</html>
