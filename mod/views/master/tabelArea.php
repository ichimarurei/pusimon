<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Kelola Pengguna</li><li>Data Master Area</li><li>Tabel</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-table"></i>
                            Tabel
                            <span>>
                                Data Master Area
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">

                                <header>
                                    <span class="widget-icon"> <i class="fa fa-th-list"></i> </span>
                                    <h2>Data Master Area</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <!--
                                            <div class="row">
                                                <form class="form-horizontal" id="model-form">
                                                    <input type="hidden" name="model-input" value="akun">
                                                    <input type="hidden" name="action-input" value="" id="ak-action-input">!--jquery--
                                                    <input type="hidden" name="key-input" value="" id="ak-key-input">!--0/ajax--
                                                    <input type="hidden" name="kode-input" value="" id="ak-kode-input">
                                                    <input type="hidden" name="terpakai-input" value="" id="ak-terpakai-input">!--ajax--
                                                    <input type="hidden" name="otoritas-input" value="lembaga">

                                                    <div class="col-sm-12">
                                                        <fieldset>

                                                            <legend><b>Tampilkan Data Kecamatan</b></legend>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Pilih Provinsi</label>
                                                                <div class="col-md-3">
                                                                    <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                                        <option value="belum">Aceh</option>
                                                                        <option value="nikah">Sumatra Utara</option>
                                                                        <option value="cerai">Riau</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button class="btn btn-info" type="submit">Saring</button>
                                                                </div>
                                                            </div>

                                                            <legend><b>Tampilkan Data Kabupaten</b></legend>
                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label">Pilih Provinsi</label>
                                                                <div class="col-md-3">
                                                                    <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                                        <option value="belum">Kecamatan 1</option>
                                                                        <option value="nikah">Kecamatan 1</option>
                                                                        <option value="cerai">Kecamatan 1</option>
                                                                    </select>
                                                                </div>
                                                                <label class="col-md-2 control-label">Pilih Kecamatan</label>
                                                                <div class="col-md-3">
                                                                    <select style="width:100%" class="select2 form-control" id="nikah-input" name="nikah-input">
                                                                        <option value="belum">Kecamatan 1</option>
                                                                        <option value="nikah">Kecamatan 1</option>
                                                                        <option value="cerai">Kecamatan 1</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button class="btn btn-info" type="submit">Saring</button>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </form>
                                            </div>
                                        -->
                                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Level</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                            <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            $(document).ready(function () {
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/area'); ?>",
                    "columns": [
                        //{'data': 'provinsi'},
                        //{'data': 'kecamatan'},
                        //{'data': 'kabupaten'},
                        {'data': 'nama'},
                        {'data': 'level'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<button class="actionBtn btn btn-info btn-flat" data-aksi="' + row.kode + '">Lihat</button>';
                            }, "targets": [2]
                        }
                    ]

                });
                var dt = $("#dt_basic").DataTable(dt_options);
                $(document).on('click', '.actionBtn', function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/master/dataArea'); ?>/" + $(this).data('aksi'));
                });
            });
        </script>
    </body>
</html>