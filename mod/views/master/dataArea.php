<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile>img {
                min-width: 151px;
                height: 170px;
                border: 5px solid #c4caca;
                margin-top: 30px;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Master</li><li>Data Area</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Data Master Area
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fas fa-pencil-alt"></i> </span>
                                    <h2>Data Master Area</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <form class="form-horizontal" id="model-form">
                                                <input type="hidden" name="model-input" value="area">
                                                <input type="hidden" name="action-input" value="" id="action-input"><!--jquery-->
                                                <input type="hidden" name="key-input" value="" id="key-input"><!--0/ajax-->
                                                <input type="hidden" name="kode-input" value="" id="kode-input">
                                                <input type="hidden" name="terpakai-input" value="" id="terpakai-input"><!--ajax-->

                                                <input type="hidden" name="induk-input" value="-" id="induk-input"><!--ajax-->


                                                <div class="col-sm-6">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Nama Wilayah*</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" placeholder="Nama Lengkap" type="text" id="nama-input" name="nama-input" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group"><label class="col-md-4 control-label">Tingkatan Administratif*</label>
                                                            <div class="col-md-8">
                                                                <select style="width:100%" class="select2 form-control" id="level-input" name="level-input">
                                                                    <option value="provinsi">Provinsi</option>
                                                                    <option value="kabupaten">Kabupaten</option>
                                                                    <option value="kecamatan">Kecamatan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group" id="fg-provinsi">
                                                        <label class="col-md-4 control-label">Provinsi*</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="provinsi-input">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="fg-kabupaten">
                                                        <label class="col-md-4 control-label">Kabupaten*</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="kabupaten-input">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>


                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a class="btn btn-default" href="<?php echo site_url('modul/tampil/master/tabelArea'); ?>">Batal</a>
                                                    <button class="btn btn-primary" type="button" id="simpan-button">
                                                        <i class="fa fa-save"></i>
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                showHideInput()
                fillInputs();
                fillInputsData('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $("#simpan-button").click(function (e) {
                    doSave("Area", "<?php echo site_url('modul/tampil/master/tabelArea'); ?>", $('#model-form'));
                });
            });

            function showHideInput() {

                $('#fg-provinsi').hide();
                $('#fg-kabupaten').hide();

                $("#level-input").change(function (e) {
                    var value = $(this).val();
                    if (value == 'provinsi') {
                        $('#fg-provinsi').hide();
                        $('#fg-kabupaten').hide();
                        $('#induk-input').val('-');
                    } else if (value == 'kabupaten') {
                        $('#fg-provinsi').show();
                        $('#fg-kabupaten').hide();
                        $('#induk-input').val($('#provinsi-input').val());
                    } else if (value == 'kecamatan') {
                        $('#fg-provinsi').show();
                        $('#fg-kabupaten').show();
                        $('#induk-input').val($('#kabupaten-input').val());
                    }
                });
            }

            function fillInputs() {
                $('#provinsi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Provinsi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/area/-'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#kabupaten-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kabupaten'});
                    }
                });

                $('#provinsi-input').change(function () {
                    $('#induk-input').val($('#provinsi-input').val());
                    var kode = $(this).val();
                    $('#kabupaten-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kabupaten'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#kabupaten-input').change(function () {
                    $('#induk-input').val($('#kabupaten-input').val());
                });
            }

            function fillInputsData(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#nama-input').val(response['data'].nama);
                        $('#induk-input').val(response['data'].induk);
                        $('#level-input').val(response['data'].level).trigger('change');

                        // Data Hidden
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#terpakai-input').val(response['data'].terpakai);

                        if (response['data'].key > 0) {
                            var pilihProvinsi = $('#provinsi-input');
                            var pilihKabupaten = $('#kabupaten-input');
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].induk_prov,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProvinsi.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProvinsi.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].induk_kab,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKabupaten.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKabupaten.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                        }
                    }
                });
            }
        </script>
    </body>
</html>