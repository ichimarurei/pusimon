<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile>img {
                min-width: 151px;
                height: 170px;
                border: 5px solid #c4caca;
                margin-top: 30px;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Kelola Pengguna</li><li>Kementrian - Lembaga</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Data Master Area
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fas fa-pencil-alt"></i> </span>
                                    <h2>Data Master Area</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <form class="form-horizontal" id="model-form">
                                            <div class="row">
                                                <input type="hidden" name="model-input" value="sarana">
                                                <input type="hidden" name="action-input" value="" id="action-input"><!--jquery-->
                                                <input type="hidden" name="key-input" value="" id="key-input"><!--0/ajax-->
                                                <input type="hidden" name="kode-input" value="" id="kode-input">
                                                <input type="hidden" name="terpakai-input" value="" id="terpakai-input"><!--ajax-->

                                                <div class="col-sm-6">
                                                    <fieldset>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Nama Bangunan</label>
                                                            <div class="col-md-8">
                                                                <input class="form-control" placeholder="Nama Lengkap" type="text" id="nama-input" name="nama-input" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Tipe</label>
                                                            <div class="col-md-8">
                                                                <select style="width:100%" class="select2 form-control" id="tipe-input" name="tipe-input">
                                                                    <option value="sekolah">Sekolah</option>
                                                                    <option value="pasar">Pasar</option>
                                                                    <option value="olga">Olahraga</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Alamat</label>
                                                            <div class="col-md-8">
                                                                <textarea class="form-control" name="alamat-input" id="alamat-input" style="width:100%" rows="8"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Provinsi</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="provinsi-input" name="provinsi-input">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Kabupaten</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="kabupaten-input" name="kabupaten-input">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Kecamatan</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="kecamatan-input" name="kecamatan-input">
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col sm-12">

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Garis Bujur (Longitude)</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Longitude" type="text" id="lon-input" name="lon-input" />
                                                    </div>
                                                    <label class="col-md-2 control-label">Garis Lintang (Latitude)</label>
                                                    <div class="col-md-4">
                                                        <input class="form-control" placeholder="Latitude" type="text" id="lat-input" name="lat-input" />
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a class="btn btn-default" href="<?php echo site_url('modul/tampil/master/tabelBangunan'); ?>">Batal</a>
                                                    <button class="btn btn-primary" type="button" id="simpan-button">
                                                        <i class="fa fa-save"></i>
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <script>
            $(document).ready(function () {
                fillInputs();
                fillInputsData('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $("#simpan-button").click(function (e) {
                    doSave("Sarana", "<?php echo site_url('modul/tampil/master/tabelBangunan'); ?>", $('#model-form'));
                });
            });

            function fillInputs() {
                $('#provinsi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Provinsi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/area/-'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#kabupaten-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kabupaten'});
                    }
                });
                
                $('#kecamatan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kecamatan'});
                    }
                });

                $('#provinsi-input').change(function () {
                    var kode = $(this).val();
                    $('#kabupaten-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kabupaten'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#kabupaten-input').change(function () {
                    var kode = $(this).val();
                    $('#kecamatan-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kecamatan'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });
            }

            function fillInputsData(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=sarana&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        // Data Pribadi
                        $('#nama-input').val(response['data'].nama);
                        $('#alamat-input').val(response['data'].alamat);
                        $('#lat-input').val(response['data'].lat);
                        $('#lon-input').val(response['data'].lon);
                        $('#tipe-input').val(response['data'].tipe).trigger('change');

                        if (response['data'].key > 0) {
                            var pilihProvinsi = $('#provinsi-input');
                            var pilihKecamatan = $('#kecamatan-input');
                            var pilihKabupaten = $('#kabupaten-input');
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].provinsi,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProvinsi.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProvinsi.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kabupaten,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKabupaten.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKabupaten.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kecamatan,
                                dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKecamatan.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKecamatan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                        }

                        // Data Hidden
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#terpakai-input').val(response['data'].terpakai);
                    }
                });
            }
        </script>
    </body>
</html>