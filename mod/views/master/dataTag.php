<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
        <style>
            .profile>img {
                min-width: 151px;
                height: 170px;
                border: 5px solid #c4caca;
                margin-top: 30px;
            }
        </style>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Data Master</li><li>Data Tag</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Data Master Tag
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fas fa-pencil-alt"></i> </span>
                                    <h2>Data Master Tag</h2>
                                </header>

                                <!-- widget div-->
                                <div>
                                    <!-- widget content -->
                                    <div class="widget-body d-flex justify-content-center">
                                        <div class="row">
                                            <form class="form-horizontal col-sm-6 col-sm-offset-3" id="model-form">
                                                <input type="hidden" name="model-input" value="tag">

                                                <input type="hidden" name="action-input" value="" id="action-input"><!--jquery-->
                                                <input type="hidden" name="key-input" value="" id="key-input"><!--0/ajax-->
                                                <input type="hidden" name="kode-input" value="" id="kode-input">
                                                <input type="hidden" name="terpakai-input" value="" id="terpakai-input"><!--ajax-->

                                                <legend>Tambah Data Tag</legend>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Nama Tag</label>
                                                    <div class="col-md-7">
                                                        <input class="form-control" placeholder="Tag" type="text" id="nama-input" name="nama-input" />
                                                    </div>
                                                    <div class="col-md-3"><button class="btn btn-info" id="simpan-button">Tambah</button></div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <legend>Tabel Data Tag</legend>
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama Tag</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>

        <script>
            $(document).ready(function () {

                fillInputsData('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $("#simpan-button").click(function (e) {
                    doSave("Tag", "<?php echo site_url('modul/tampil/master/tabelTag'); ?>", $('#model-form'));
                });

                $(document).on('click', '.btn-edit', function () {
                    $(location).attr('href', "<?php echo site_url('modul/tampil/master/dataTag'); ?>/" + $(this).data('aksi'));
                });
                
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/tag'); ?>",
                    "columns": [
                        {'data': 'nama'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<button class="btn btn-info btn-flat btn-edit" data-aksi="' + row.kode + '">Ubah</button>';
                            }, "targets": [1]
                        }
                    ]

                });
                var dt = $("#dt_basic").DataTable(dt_options);

            });
            
            function fillInputsData(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=tag&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#nama-input').val(response['data'].nama);

                        // Data Hidden
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#terpakai-input').val(response['data'].terpakai);

                    }
                });
            }
        </script>
    </body>
</html>