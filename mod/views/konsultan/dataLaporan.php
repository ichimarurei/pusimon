<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// @author Muhammad Iqbal (市丸 零)
?>
<!DOCTYPE html>
<html lang="en-us">
    <head>
        <?php include_once(APPPATH . '/views/_html/_head.php'); ?>
    </head>
    <body class="smart-style-1 fixed-header fixed-navigation fixed-ribbon menu-on-top">
        <?php include_once(APPPATH . '/views/_html/_bodyTop.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_bodyNav.php'); ?>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Konsultan Lapangan</li><li>Laporan</li><li>Pendataan</li>
                </ol>
                <!-- end breadcrumb -->
            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <!-- PAGE HEADER -->
                            <i class="fa-fw fa fa-columns"></i>
                            Pendataan
                            <span>>
                                Laporan Konsultan Lapangan
                            </span>
                        </h1>
                    </div>
                    <!-- end col -->

                    <!-- right side of the page with the sparkline graphs -->
                    <!-- col -->
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->

                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <!-- row -->
                    <div class="row">
                        <!-- NEW WIDGET START -->
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-0"
                                 data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false"
                                 data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-collapsed="false"
                                 data-widget-sortable="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-pencil-alt"></i> </span>
                                    <h2>Laporan Konsultan Lapangan</h2>
                                </header>

                                <!-- widget content -->
                                <div class="widget-body">

                                    <form class="form-horizontal" id="model-form">

                                        <input type="hidden" name="model-input" value="laporkonsul">
                                        <input type="hidden" name="action-input" value="" id="action-input"><!--jquery-->
                                        <input type="hidden" name="key-input" value="" id="key-input"><!--0/ajax-->
                                        <input type="hidden" name="kode-input" value="" id="kode-input">
                                        <input type="hidden" name="terpakai-input" value="" id="terpakai-input"><!--ajax-->
                                        
                                        <input type="hidden" name="urut-input" value="" id="urut-input"><!--ajax-->
                                        <input type="hidden" name="bangunan-input" value="" id="bangunan-input"><!--ajax-->
                                                    
                                                    
                                        <fieldset>
                                            <legend><b>Saring Data Konsultan</b></legend>
                                            <div class="form-group">
                                                <label for="batch-input" class="col-md-2 control-label">Batch Report</label>
                                                <div class="col-md-4"><input type="text" class="form-control" name="nomor-input" id="nomor-input" readonly ></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="konsultan-input" class="col-md-2 control-label">Konsultan</label>
                                                <div class="col-md-4">
                                                    <select style="width:100%" class="select2 form-control" id="konsultan-input" name="konsultan-input">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Sarana</label>
                                                <div class="col-md-4">
                                                    <select style="width:100%" class="select2 form-control" id="sarana-input" name="sarana-input">
                                                        <option value="sekolah">Sekolah</option>
                                                        <option value="pasar">Pasar</option>
                                                        <option value="olahraga">Olah Raga</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Lokasi</label>
                                                <div class="col-md-3">
                                                    <select style="width:100%" class="select2 form-control" id="provinsi-input" name="provinsi-input">
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <select style="width:100%" class="select2 form-control" id="kabupaten-input" name="kabupaten-input">
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <select style="width:100%" class="select2 form-control" id="kecamatan-input" name="kecamatan-input">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-1 col-sm-offset-10">
                                                    <input class="btn btn-info" style="width:100%" type="button" value="Saring" id="saring-konsultan-input">
                                                </div>
                                            </div>
                                        </fieldset>
                                    
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <legend><b>Hasil Pencarian Data Konsultan</b></legend>
                                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama Bangunan</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>

                                            <div class="col-md-6 order-1">
                                                    
                                                    <legend><b>Data Detail Laporan Konsultan</b></legend>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pimpinan</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared" placeholder="Nama Pimpinan" type="text" id="pemimpin-input" name="pemimpin-input" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Alamat</label>
                                                        <div class="col-md-8">
                                                            <textarea class="form-control text-tetap can-cleared" placeholder="Alamat Lokasi" rows="4" id="alamat-input"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Garis Lintang (Latitude)</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared" placeholder="Koordinat Lokasi" type="text" id="lat-input"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Garis Bujur (Longitude)</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared" placeholder="Koordinat Lokasi" type="text" id="lon-input"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Tanggal Survai</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared" placeholder="Tanggal Survei" type="text" id="tanggal_survei-input" name="tanggal_survei-input" data-provide="datepicker" data-date-format="dd/mm/yyyy" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Kondisi</label>
                                                        <div class="col-md-8">
                                                            <select style="width:100%" class="select2 form-control" id="kondisi-input" name="kondisi-input">
                                                                <option value="rusak berat">Rusak Berat</option>
                                                                <option value="tidak rusak berat">Tidak Rusak Berat</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Unggah Foto</label>
                                                        <input type="hidden" name="lampiran_foto-input" id="lampiran_foto-input" value="">
                                                        <div class="col-md-8">
                                                            <button class="btn btn-default unggah-button"  data-untuk="survei" type="button" data-toggle="modal" data-target="#modal-foto">Unggah Foto</button>  
                                                            <button class="btn btn-default unggah-button" data-untuk="foto" type="button" data-toggle="modal" data-target="#modal-lihat-foto">Lihat Foto</button>
                                                        </div>
                                                    </div>
                                                    
                                                    <legend><b>Dokumen Pendukung</b></legend>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Tanggal Penyusunan</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared" placeholder="Tanggal Penyusunan" type="text" id="tanggal_susun-input" name="tanggal_susun-input" data-provide="datepicker" data-date-format="dd/mm/yyyy" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Nilai Kontruksi</label>
                                                        <div class="col-md-8">
                                                            <input class="form-control can-cleared nominal-text" placeholder="Nilai Kontruksi" type="text" id="nominal-input" name="nominal-input" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Unggah Berkas</label>
                                                        <div class="col-md-8">
                                                            <div class="col-md-3" style="padding-left:0px"><button class="btn btn-default unggah-button" style="width:100%" data-untuk="survei" type="button" data-toggle="modal" data-target="#modal-rks">Doc. RKS</button></div>
                                                            <div class="col-md-4" style="padding:0px"><button class="btn btn-default unggah-button" style="width:100%" data-untuk="survei" type="button" data-toggle="modal" data-target="#modal-rab">Doc. DED & RAB</button></div>
                                                            <div class="col-md-5" style="padding-right:0px"><button class="btn btn-default unggah-button" style="width:100%" data-untuk="survei" type="button" data-toggle="modal" data-target="#modal-upl">Doc. UPL/UKL/AMDAL</button></div>
                                                            
                                                            <input type="hidden" value="" id="lampiran_rks-input" name="lampiran_rks-input">
                                                            <input type="hidden" value="" id="lampiran_rab-input" name="lampiran_rab-input">
                                                            <input type="hidden" value="" id="lampiran_upl-input" name="lampiran_upl-input">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-2 col-md-offset-10">
                                                            <input type="button" style="width:100%" class="form-control btn btn-info" id="simpan-laporan-konsultan" value="Simpan">
                                                        </div>
                                                    </div>  
                                            </div>
                                        </div>

                                    </form>

                                    <legend><b>Hasil Laporan</b></legend>
                                    <table id="dt_basic_hasil" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sarana</th>
                                                <th>Pimpinan</th>
                                                <th>Lokasi</th>
                                                <th>Tanggal Survai</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a class="btn btn-default" href="<?php echo site_url('modul/tampil/akun/tabelBioKonsultan'); ?>">Batal</a>
                                                <button class="btn btn-primary" type="button" id="simpan-button">
                                                    <i class="fa fa-save"></i>
                                                    Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget -->
                        </article>
                        <!-- WIDGET END -->
                    </div>
                    <!-- end row -->

                    <!-- row -->
                    <div class="row">
                        <!-- a blank row to get started -->
                        <div class="col-sm-12">
    
                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-foto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedoc-foto" name="filedocument[]" class="file-loading" type="file" accept="image/*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-rks">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedoc-rks" name="filedocument[]" class="file-loading" type="file" accept="*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-rab">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedoc-ded" name="filedocument[]" class="file-loading" type="file" accept="*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-upl">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input id="filedoc-upl" name="filedocument[]" class="file-loading" type="file" accept="*" multiple>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Modal Untuk Foto User -->
                            <div class="modal fade" id="modal-lihat-foto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Upload Photo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row" id="card-foto">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        <!-- your contents here -->
                        </div>
                    </div>
                    <!-- end row -->
                </section>
                <!-- end widget grid -->

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->

        <?php include_once(APPPATH . '/views/_html/_bodyFoot.php'); ?>
        <!-- ================================================== -->
        <?php include_once(APPPATH . '/views/_html/_script.php'); ?>
        <?php include_once(APPPATH . '/views/_html/_datatables.php'); ?>
        <script>
            var tabelnya;
            var tabellaporkonsul

            $(document).ready(function () {
                
                tabellaporkonsul = tableLaporKonsul();
                tabelnya = tablePencarian();

                fillWilayahInputs();
                fillInputsData('<?php echo ($this->uri->segment(5) === FALSE) ? 0 : $this->uri->segment(5); ?>');

                $('#saring-konsultan-input').click(function(e){
                    if ($('#provinsi-input').val() !== null && $('#kabupaten-input').val() !== null && $('#kecamatan-input').val() != null) {
                        var urlParam = $('#sarana-input').val() + '___d___' + $('#kecamatan-input').val();
                        tabelnya.ajax.url("<?php echo site_url('data/tabel/sarana'); ?>/" + urlParam).load();
                        tabelnya.columns.adjust().draw();
                    }
                });
                
                $(document).on('click', '.actionBtn', function () {
                    fillInputsSarana($(this).data('aksi'));
                });

                $("#simpan-laporan-konsultan").click(function(e){
                    
                    nominalFormats();
                    doSaveLaporan($('#model-form'));
                    
                    tabellaporkonsul.ajax.url("<?php echo site_url('data/tabel/laporkonsul'); ?>/" + $('#nomor-input').val()).load();
                    tabellaporkonsul.columns.adjust().draw();
                    
                    $(".can-cleared").val("");
                    generateKodeBaru();

                });

                $(".nominal-text").keyup(function () {
                    var text = formatRp($(this).val());
                    $(this).val(text);
                });

            });

            function doSaveLaporan(form) {
                return $.ajax({
                    url: "<?php echo site_url('data/simpan'); ?>", data: form.serialize(),
                    "async": false, dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        var title = 'Success';
                        var message = '';
                        var icon = 'warning';

                        if (response['return'].code === 0) {
                            title = 'Eror';
                            icon = 'error';
                            message = 'Error !!!';
                        } else if (response['return'].code === 1) {
                            title = 'Berhasil';
                            icon = 'success';
                            message = 'Data Berhasil ' + text;
                        } else if (response['return'].code === 2) {
                            title = 'Gagal';
                            icon = 'error';
                            message = 'Data Gagal ' + text;
                        } else if (response['return'].code === 3) {
                            title = 'Perhatian !!!';
                            icon = 'warning';
                            message = response['return'].message;
                        }

                        swal({
                            title: title,
                            html: message,
                            timer: 3000,
                            type: icon,
                            showConfirmButton: false,
                            onClose: function () {
                                $.unblockUI();

                                if (response['return'].code === 1) {
                                    $(location).attr('href', url);
                                }
                            }
                        });
                    }
                });
            }

            function tablePencarian() {
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/sarana/none'); ?>",
                    "columns": [
                        {'data': 'nama'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<input type="button" class="actionBtn btn btn-info btn-flat" data-aksi="' + row.kode + '" value="Pilih">';
                            }, "targets": [1]
                        }
                    ]
                });

                return $("#dt_basic").DataTable(dt_options);
            }

            function tableLaporKonsul() {
                var dt_options = $.extend({}, globalDTOptions, {
                    "ajax": "<?php echo site_url('data/tabel/laporkonsul'); ?>/none",
                    "async": false,
                    "columns": [
                        {'data': 'sarana'},
                        {'data': 'pemimpin'},
                        {'data': 'alamat'},
                        {'data': 'tanggal_survei'},
                        {'width': '7%'}
                    ], "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return '<input type="button" class="actionBtn btn btn-info btn-flat" data-aksi="' + row.kode + '" value="Pilih">';
                            }, "targets": [4]
                        }
                    ]
                });

                return $("#dt_basic_hasil").DataTable(dt_options);
            }

            function generateKodeBaru() {
                $.ajax({
                    url: "<?php echo site_url('data/kodean'); ?>",
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#kode-input').val(response['data']);
                    }
                });
            }

            function fillInputsData(param) {

                $('#konsultan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Konsultan'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/akun/konsultan'); ?>",
                        dataType: 'json', type: 'POST', cache: false
                    }
                });

                $.ajax({
                    
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=laporkonsul&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        
                        $('#urut-input').val(response['data'].urut);
                        $('#nomor-input').val(response['data'].nomor);
                        $('#pemimpin-input').val(response['data'].pemimpin);
                        $('#sarana-input').val(response['data'].sarana);
                        $('#bangunan-input').val(response['data'].bangunan);
                        
                        $('#tanggal_survei-input').val(response['data'].tanggal_survei);
                        $('#tanggal_susun-input').val(response['data'].tanggal_susun);
                        $('#nominal-input').val(response['data'].nominal);
                        $('#kondisi-input').val(response['data'].kondisi);

                        if (response['data'].key > 0) {
                            var pilihKonsultan = $('#konsultan-input');

                            var pilihProvinsi = $('#provinsi-input');
                            var pilihKecamatan = $('#kecamatan-input');
                            var pilihKabupaten = $('#kabupaten-input');

                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>",data: 'param=akun&kode=' + response['data'].konsultan,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKonsultan.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKonsultan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].provinsi,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihProvinsi.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihProvinsi.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kabupaten,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKabupaten.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKabupaten.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });
                            
                            $.ajax({
                                url: "<?php echo site_url('data/detail'); ?>", data: 'param=area&kode=' + response['data'].kecamatan,
                                async:false, dataType: 'json', type: 'POST', cache: false
                            }).then(function (data) {
                                // create the option and append to Select2
                                pilihKecamatan.append(new Option(data['data'].nama, data['data'].kode, true, true)).trigger('change');
                                // manually trigger the `select2:select` event
                                pilihKecamatan.trigger({
                                    type: 'select2:select',
                                    params: {
                                        data: data['data']
                                    }
                                });
                            });

                            var urlParam = response['data'].sarana + '___d___' + response['data'].kecamatan;
                            tabelnya.ajax.url("<?php echo site_url('data/tabel/sarana'); ?>/" + urlParam).load();
                            tabelnya.columns.adjust().draw();


                            tabellaporkonsul.ajax.url("<?php echo site_url('data/tabel/laporkonsul'); ?>/" + $('#nomor-input').val()).load();
                            tabellaporkonsul.columns.adjust().draw();

                            var arrayFoto =  response['data'].lampiran_foto.split('___');

                            $.each(arrayFoto, function (index, value){
                                console.log(value);
                                var card = 
                                '<div class="col-md-4" style="margin-bottom:10pt">'+
                                    '<img src="<?php echo site_url('file/app/lampiran'); ?>/'+ response['data'].kode +'/'+value +'" '+
                                    'class="card-img-top" style="width:100%" alt="...">'+
                                '</div>';

                                $("#card-foto").append(card);
                                
                                var card = 
                                '<div class="col-md-4" style="margin-bottom:10pt">'+
                                    '<img src="<?php echo site_url('file/app/lampiran'); ?>/'+ response['data'].kode +'/'+value +'" '+
                                    'class="card-img-top" style="width:100%" alt="...">'+
                                '</div>';

                                $("#card-foto").append(card);
                                var card = 
                                '<div class="col-md-4" style="margin-bottom:10pt">'+
                                    '<img src="<?php echo site_url('file/app/lampiran'); ?>/'+ response['data'].kode +'/'+value +'" '+
                                    'class="card-img-top" style="width:100%" alt="...">'+
                                '</div>';

                                $("#card-foto").append(card);
                                
                                var card = 
                                '<div class="col-md-4" style="margin-bottom:10pt">'+
                                    '<img src="<?php echo site_url('file/app/lampiran'); ?>/'+ response['data'].kode +'/'+value +'" '+
                                    'class="card-img-top" style="width:100%" alt="...">'+
                                '</div>';

                                $("#card-foto").append(card);
                            });
                        }

                        // Data Hidden
                        $('#action-input').val((response['data'].key === 0) ? 1 : 2);
                        $('#key-input').val(response['data'].key);
                        $('#kode-input').val(response['data'].kode);
                        $('#terpakai-input').val(response['data'].terpakai);
                    }

                });

            }
            
            function fillWilayahInputs() {
                $('#provinsi-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Provinsi'});
                    },
                    ajax: {
                        url: "<?php echo site_url('data/pilih/area/-'); ?>",
                        async:true, dataType: 'json', type: 'POST', cache: false
                    }
                });

                $('#kabupaten-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kabupaten'});
                    }
                });

                $('#kecamatan-input').select2({
                    initSelection: function (element, callback) {
                        return callback({id: '', text: 'Pilih Kecamatan'});
                    }
                });

                $('#provinsi-input').change(function () {
                    var kode = $(this).val();
                    $('#kabupaten-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kabupaten'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

                $('#kabupaten-input').change(function () {
                    var kode = $(this).val();
                    $('#kecamatan-input').select2({
                        initSelection: function (element, callback) {
                            return callback({id: '', text: 'Pilih Kecamatan'});
                        },
                        ajax: {
                            url: "<?php echo site_url('data/pilih/area'); ?>/" + kode,
                            dataType: 'json', type: 'POST', cache: false
                        }
                    });
                });

            }

            function fillInputsSarana(param) {
                $.ajax({
                    url: "<?php echo site_url('data/detail'); ?>", data: 'param=sarana&kode=' + param,
                    dataType: 'json', type: 'POST', cache: false,
                    success: function (response) {
                        $('#alamat-input').val(response['data'].alamat);
                        $('#lon-input').val(response['data'].lon);
                        $('#lat-input').val(response['data'].lat);

                        $('#bangunan-input').val(param);
                    }
                });
                
            }

            
        </script>

        <script>

            var filename = [];

            $(document).ready(function () {
                
                initUploader('#filedoc-foto', 'file/app/lampiran', 3);
                initUploader('#filedoc-rks', 'file/app/lampiran');
                
                initUploader('#filedoc-ded', 'file/app/lampiran');
                initUploader('#filedoc-upl', 'file/app/lampiran');
                
                $('#modal-foto').on('hidden.bs.modal', function () {
                    var new_filename = filename.join('___');
                    filename = [];
                    $('#lampiran_foto-input').val(new_filename);
                    console.log(new_filename);
                });

                $('#modal-rks').on('hidden.bs.modal', function () {
                    $('#lampiran_rks-input').val(filename[0])
                    filename = [];
                    console.log(filename[0]);
                });

                $('#modal-rab').on('hidden.bs.modal', function () {
                    $('#lampiran_rab-input').val(filename[0])
                    filename = [];
                    console.log($('#lampiran_rab-input').val());
                });

                $('#modal-upl').on('hidden.bs.modal', function () {
                    $('#lampiran_upl-input').val(filename[0])
                    filename = [];
                    console.log($('#lampiran_upl-input').val());
                });

                
            });

            function initUploader(id, path_file, max_upload=1) {
                
                $(id).fileinput({
                    maxFileCount: max_upload, uploadUrl: "<?php echo site_url('data/unggah'); ?>", // your upload server url
                    browseClass: "btn btn-default btn-flat", browseLabel: "Browse", browseIcon: '<i class="fa fa-folder-open"></i> ',
                    removeClass: "btn btn-warning btn-flat", removeLabel: "Delete", removeIcon: '<i class="fas fa-trash"></i> ',
                    uploadClass: "btn btn-primary btn-flat", uploadLabel: "Upload", uploadIcon: '<i class="fa fa-cloud-upload-alt"></i> ',
                    showCaption: false,
                    uploadExtraData: function () {
                        return {path: path_file, dir: $('#kode-input').val()};
                    }
                });

                $(id).on('fileloaded', function (event, file, previewId, index, reader) {
                    filename.push(file.name.replace(/\s/g, '_'));
                });

            }

        </script>
    </body>
</html>